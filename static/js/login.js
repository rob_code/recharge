/***************************************************
 ** @Desc : This file for 登录js
 ** @Time : 2019.04.01 13:34
 ** @Author : Joker
 ** @File : login.js
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.01 13:34
 ** @Software: GoLand
 ****************************************************/
let login = {
    //点击刷新验证码
    changeImg: function () {
        login.setImgSrc($("#rcCaptcha-img"), "reload=" + (new Date()).getDate());
    },
    setImgSrc: function (obj, reload) {
        var $src = obj[0].src;
        var $flag = $src.indexOf("?");
        if ($flag >= 0) {
            $src = $src.substr(0, $flag);
        }
        obj.attr("src", $src + "?" + reload);
    },
    //验证输入的验证码
    verifyCaptchaId: function () {
        $.ajax({
            type: "GET",
            url: "/verifyCaptcha.py/" + $("#captchaCode").val() + "/" + $("#captchaId").val(),
            success: function (res) {
                if (res.code === -9) {
                    toastr.error(res.msg, function () {
                        login.flushCaptcha();
                    });
                } else {
                    $("#verify_flag").val("1");
                }
            }
        });
    },
    //自动刷新验证码
    flushCaptcha: function () {
        $.ajax({
            type: "GET",
            url: "flushCaptcha.py",
            success: function (res) {
                $("#rcCaptcha-img").attr("src", "/captcha/" + res.data + ".png");
                $("#captchaId").val(res.data);
            }
        });
    },
    //获取手机验证码
    getPhone: function () {
        let userName = $("#userName").val();
        let Password = $("#Password").val();
        // let captchaCode = $("#captchaCode").val();
        if (userName === "" || Password === "") {
            toastr.error("用户名或者密码不能为空！");
        } /*else if (captchaCode === "") {
            toastr.error("验证码不能为空！");
        }*/ else {
            $.ajax({
                type: "POST",
                url: "/sms.do/",
                data: $("#login_form").serialize(),
                success: function (data) {
                    if (data.code === -9) {
                        toastr.error(data.msg);
                        setInterval(function () {
                            window.location.reload();
                        }, 2000)
                    } else {
                        toastr.success(data.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    login_action: function () {
        let userName = $("#userName").val();
        let Password = $("#Password").val();
        let captchaCode = $("#captchaCode").val();
        let phoneCode = $("#phoneCode").val();
        if (userName === "" || Password === "") {
            toastr.error("用户名或者密码不能为空！");
        } else if (captchaCode === "" || phoneCode === "") {
            toastr.error("验证码不能为空！");
        } else {
            $.ajax({
                type: "POST",
                url: "/login.py/",
                data: $("#login_form").serialize(),
                success: function (data) {
                    if (data.code === -9) {
                        toastr.error(data.msg);
                        setInterval(function () {
                            window.location.reload();
                        }, 2000)
                    } else {
                        window.location.href = data.url;
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    loginout: function () {
        swal({
            title: "Are you sure?",
            text: "您确定要退出登录吗？",
            icon: "warning",
            closeOnClickOutside: false,
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "get",
                    url: "/loginOut.py",
                    success: function (res) {
                        window.location.href = res.url;
                    },
                    error: function (XMLHttpRequest) {
                        toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                    }
                });
            }
        });
    }
};