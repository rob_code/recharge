/***************************************************
 ** @Desc : This file for 商户通道列表分页js
 ** @Time : 2019.04.02 11:11
 ** @Author : Joker
 ** @File : merchant_page
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.02 11:11
 ** @Software: GoLand
 ****************************************************/

let do_paging = {
    xf_merchant: function () {
        let merchantNo = $("#merchant_No").val();
        let merchantName = $("#merchant_Name").val();
        $.ajax({ //去后台查询第一页数据
            type: "GET",
            url: "/merchant/list_xf/",
            data: {
                page: '1',
                limit: "20",
                MerchantNo: merchantNo,
                MerchantName: merchantName,
            }, //参数：当前页为1
            success: function (data) {
                do_paging.show_merchant_data(data.root);//处理第一页的数据

                let options = {//根据后台返回的分页相关信息，设置插件参数
                    bootstrapMajorVersion: 3, //
                    currentPage: data.page, //当前页数
                    totalPages: data.totalPage, //总页数
                    numberOfPages: data.limit,//每页记录数
                    itemTexts: function (type, page) {//设置分页按钮显示字体样式
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//分页按钮点击事件
                        $.ajax({//根据page去后台加载数据
                            url: "/merchant/list_xf/",
                            type: "GET",
                            data: {
                                page: page,
                                MerchantNo: merchantNo,
                                MerchantName: merchantName,
                            },
                            success: function (data) {
                                do_paging.show_merchant_data(data.root);//处理数据
                            }
                        });
                    }
                };
                $('#xf_page').bootstrapPaginator(options);//设置分页
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    show_merchant_data: function (list) {
        let con = "";
        $.each(list, function (index, item) {
            con += "<tr><td><a>" + (index + 1) + "</a></td>";
            con += "<td>" + item.MerchantName + "</td>";
            con += "<td>" + item.MerchantNo + "</td>";
            con += "<td>" + item.TotalAmount + "</td>";
            con += "<td>" + item.FrozenAmount + "</td>";
            con += "<td>" + item.UsableAmount + "</td>";
            con += "<td>" + item.EditTime + "</td>";
            con += "<td><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"xf_merchant.edit_merchant_ui(" + item.Id + ");\" title=\"编辑\"><i class=\"fa fa-pencil\"></i>编辑</button>";
            con += "&nbsp;<button type=\"button\" class=\"btn btn-danger btn-xs\" onclick=\"xf_merchant.del_account(" + item.Id + ");\" title=\"删除\"><i class=\"fa fa-trash-o\"></i>删除</button>";
            con += "&nbsp;<button type=\"button\" class=\"btn btn-info btn-xs\" onclick=\"xf_merchant.query_balance(" + item.Id + ");\" title=\"更新金额\"><i class=\"glyphicon glyphicon-inbox\"></i>更新金额</button>";
            con += "</td></tr>";
        });
        $("#your_showtime").html(con);
    }
};