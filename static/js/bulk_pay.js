/***************************************************
 ** @Desc : This file for 批量代付js
 ** @Time : 2019.04.14 11:23
 ** @Author : Joker
 ** @File : bulk_pay
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.14 11:23
 ** @Software: GoLand
 ****************************************************/

let bulk_pay = {
    form_check: function () {
        $("#btn_submit_edit").hide();

        let all_amount = $("#all_amount").val();
        let all_records = $("#all_records").val();
        let mobileNo = $("#mobileNo").val();
        let file = $("#file").val();
        let mobileCode = $("#mobileCode").val();

        let patrn = /^[1-9]\d*$/;
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
        let patrn3 = /^[1]([3-9])[0-9]{9}$/;

        let pos = file.lastIndexOf(".");
        let lastname = file.substring(pos, file.length);

        let bulk_pay_form = document.getElementById('bulk_pay_form'),
            formData = new FormData(bulk_pay_form);

        if (!patrn2.exec(all_amount)) {
            toastr.error("请输入正确的金额哦");
            $("#btn_submit_edit").show();
        } else if (!patrn.exec(all_records)) {
            toastr.error("请输入正确的笔数哦!");
            $("#btn_submit_edit").show();
        } else if (!patrn3.exec(mobileNo)) {
            toastr.error("请输入正确的手机号!");
            $("#btn_submit_edit").show();
        } else if (lastname.toLowerCase() !== ".xls" && lastname.toLowerCase() !== ".xlsx") {
            toastr.error("仅支持“xls”、“xlsx”格式文件!");
            $("#btn_submit_edit").show();
        } else if (mobileCode === "") {
            toastr.error("手机验证码不能为空!");
            $("#btn_submit_edit").show();
        } else {
            $.ajax({
                type: "POST",
                url: "/merchant/do_bulk_pay/",
                data: formData,
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res.code === 9) {
                        swal("提交成功！", {
                            icon: "success",
                            closeOnClickOutside: false,
                        }).then(() => {
                            $("#btn_submit_edit").show();
                        });
                    } else {
                        swal(res.msg, {
                            icon: "warning",
                            closeOnClickOutside: false,
                        }).then(() => {
                            $("#btn_submit_edit").show();
                        });
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                    $("#btn_submit_edit").show();
                }
            });
        }
    },
    send_msg_do_bulk_pay: function () {
        let all_amount = $("#all_amount").val();
        let all_records = $("#all_records").val();
        let mobileNo = $("#mobileNo").val();
        let file = $("#file").val();

        let patrn = /^[1-9]\d*$/;
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
        let patrn3 = /^[1]([3-9])[0-9]{9}$/;

        let pos = file.lastIndexOf(".");
        let lastname = file.substring(pos, file.length);

        let bulk_pay_form = document.getElementById('bulk_pay_form'),
            formData = new FormData(bulk_pay_form);

        if (!patrn2.exec(all_amount)) {
            toastr.error("请输入正确的金额哦");
        } else if (!patrn.exec(all_records)) {
            toastr.error("请输入正确的笔数哦!");
        } else if (!patrn3.exec(mobileNo)) {
            toastr.error("请输入正确的手机号!");
        } else if (lastname.toLowerCase() !== ".xls" && lastname.toLowerCase() !== ".xlsx") {
            toastr.error("仅支持“xls”、“xlsx”格式文件!");
        } else {
            $.ajax({
                type: "POST",
                url: "/merchant/do_bulk_pay_sms/",
                data: formData,
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res.code === 9) {
                        toastr.success(res.msg);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    bulk_recharge_form_check: function () {
        $("#btn_submit_edit").hide();

        let all_amount = $("#all_amount").val();
        let all_records = $("#all_records").val();
        let file = $("#file").val();

        let patrn = /^[1-9]\d*$/;
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

        let pos = file.lastIndexOf(".");
        let lastname = file.substring(pos, file.length);

        let bulk_pay_form = document.getElementById('bulk_pay_form'),
            formData = new FormData(bulk_pay_form);

        if (!patrn2.exec(all_amount)) {
            toastr.error("请输入正确的金额哦");
            $("#btn_submit_edit").show();
        } else if (!patrn.exec(all_records)) {
            toastr.error("请输入正确的笔数哦!");
            $("#btn_submit_edit").show();
        } else if (lastname.toLowerCase() !== ".xls" && lastname.toLowerCase() !== ".xlsx") {
            toastr.error("仅支持“xls”、“xlsx”格式文件!");
            $("#btn_submit_edit").show();
        }  else {
            $.ajax({
                type: "POST",
                url: "/merchant/do_bulk_recharge/",
                data: formData,
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res.code === 9) {
                        swal("提交成功！", {
                            icon: "success",
                            closeOnClickOutside: false,
                        }).then(() => {
                            $("#btn_submit_edit").show();
                        });
                    } else {
                        swal(res.msg, {
                            icon: "warning",
                            closeOnClickOutside: false,
                        }).then(() => {
                            $("#btn_submit_edit").show();
                        });
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                    $("#btn_submit_edit").show();
                }
            });
        }
    },
};