/***************************************************
 ** @Desc : This file for 发送短信验证码
 ** @Time : 2019.04.04 9:37
 ** @Author : Joker
 ** @File : send_messages
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.04 9:37
 ** @Software: GoLand
****************************************************/
package utils

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"recharge/sys"
)

type SendMessages struct{}

//发送验证码
func (*SendMessages) SendSmsCode(mobile, code string) bool {
	tplValue := url.Values{"#code#": {code}}.Encode()

	dataTplSms := url.Values{
		"apikey":    {APIKEY},
		"mobile":    {mobile},
		"tpl_id":    {fmt.Sprintf("%d", TPL1)},
		"tpl_value": {tplValue}}
	err := httpPostForm(URLSENDSMS, dataTplSms)
	if err != nil {
		sys.LogError("SendSmsCode fail to send ,err:", err)
		return false
	}
	sys.LogInfo("sms send success")
	return true
}

//发送提现通知
func (*SendMessages) SendSmsForPay(mobile, code string) bool {
	tplValue := url.Values{"#code#": {code}}.Encode()
	dataTplSms := url.Values{
		"apikey":    {APIKEY},
		"mobile":    {mobile},
		"tpl_id":    {fmt.Sprintf("%d", TPL2)},
		"tpl_value": {tplValue}}
	err := httpPostForm(URLSENDSMS, dataTplSms)
	if err != nil {
		sys.LogError("SendSmsCode fail to send ,err:", err)
		return false
	}
	sys.LogInfo("sms send success")
	return true
}

func httpPostForm(url string, data url.Values) error {
	resp, err := http.PostForm(url, data)
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	sys.LogInfo("sms response:", string(body))

	defer resp.Body.Close()
	return nil
}
