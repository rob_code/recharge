# recharge

#### 一. 介绍

1. 基于beego框架
2. 充值代付平台（实现了充值管理，代付管理，用户管理，权限管理，网关，回调，对账，分润）
3. 本项目已稳定运行过半年，高峰时每分钟处理过40笔订单
4. 该开源项目只能用于学习，不准备用于任何的非法商业活动，否则后果自负


#### 二. 软件架构
##### 项目目录结构说明

    conf：app.conf：runmode=dev表示开发模式，=pro表示运营模式，二者不能同时存在；httpport是端口号，任意设置，不冲突即可
    controllers：控制器
    logs：存放日志文件
    routers：路由文件
    static：css/js/图片文件
    sys：系统常量配置、session配置
    utils：数据库安装文件、日志配置、短信对接云片
    views：html文件

#### 三. 安装教程
##### 运行环境
Go 1.10 - 1.14 版本；
机器最低配置： 2核CPU，4G内存，32/64位系统均可；
mysql 5.0 以上版本；

##### 在win环境下
1. 创建数据库：payfor_sys(数据库名任意，只要保证与app.conf文件中的db_name一致即可)，导入sql文件：payfor_sys_back.sql（可以不导入sql文件，只要保证数据库连接信息正确，项目会自动创建表结构和生成测试数据，测试账户：“Joker”，密码：“Joker.”，此账号拥有最高系统权限）
2. 进入recharge目录，进入此目录下的命令行模式
3. 输入go build，会在recharge目录下生成一个recharge.exe文件，点击即可运行
4. 在地址栏输入http://localhost:port/即可运行，若出现404，重新进行第3步
5. 登录已经注释掉了短信验证功能，验证码输入任意字串即可

##### 在windows下编译成Linux下可执行的二进制文件并且执行

1. 在win环境下，进入recharge目录，进入此目录下的命令行模式
2. 执行下面的命令：set GOARCH=amd64；set GOOS=linux
3. 输入go build，会在jhagent目录下生成一个没有后缀的recharge二进制文件
4. 将该文件放入linux系统某个文件夹下
5. 赋予文件权限：chmod 777 recharge；执行：./recharge
6. 二进制文件不需要go的任何依赖，可以直接运行
7. 在地址栏输入http://localhost:post/即可运行，若出现404，重新进行第3步

#### 项目截图

1.  首页1
![首页1](https://gitee.com/Joker_zero/images/raw/master/recharge/%E9%A6%96%E9%A1%B51.png "首页1")
2.  首页2
![首页2](https://gitee.com/Joker_zero/images/raw/master/recharge/%E9%A6%96%E9%A1%B52.png "首页2")
3.  登录
![登录](https://gitee.com/Joker_zero/images/raw/master/recharge/%E7%99%BB%E5%BD%95.png "登录")
4.  首页3
![首页3](https://gitee.com/Joker_zero/images/raw/master/recharge/%E9%A6%96%E9%A1%B53.png "首页3")
5.  充值记录
![充值记录](https://gitee.com/Joker_zero/images/raw/master/recharge/%E5%85%85%E5%80%BC%E8%AE%B0%E5%BD%95.png "充值记录")
6.  代付记录
![代付记录](https://gitee.com/Joker_zero/images/raw/master/recharge/%E4%BB%A3%E4%BB%98%E8%AE%B0%E5%BD%95.png "代付记录")
7.  对账管理
![对账管理](https://gitee.com/Joker_zero/images/raw/master/recharge/%E5%AF%B9%E8%B4%A6%E7%AE%A1%E7%90%86.png "对账管理")
8.  批量充值
![批量充值](https://gitee.com/Joker_zero/images/raw/master/recharge/%E6%89%B9%E9%87%8F%E5%85%85%E5%80%BC.png "批量充值")
9.  批量代付
![批量代付](https://gitee.com/Joker_zero/images/raw/master/recharge/%E6%89%B9%E9%87%8F%E4%BB%A3%E4%BB%98.png "批量代付")
10. 商户列表
![商户列表](https://gitee.com/Joker_zero/images/raw/master/recharge/%E5%95%86%E6%88%B7%E5%88%97%E8%A1%A8.png "商户列表")
11. 线下充值
![线下充值](https://gitee.com/Joker_zero/images/raw/master/recharge/%E7%BA%BF%E4%B8%8B%E5%85%85%E5%80%BC.png "线下充值")
12. 修改个人信息
![修改个人信息](https://gitee.com/Joker_zero/images/raw/master/recharge/%E4%BF%AE%E6%94%B9%E4%B8%AA%E4%BA%BA%E4%BF%A1%E6%81%AF.png "修改个人信息")
13. 用户管理
![用户管理](https://gitee.com/Joker_zero/images/raw/master/recharge/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.png "用户管理")
14. 在线代付
![在线代付](https://gitee.com/Joker_zero/images/raw/master/recharge/%E5%9C%A8%E7%BA%BF%E4%BB%A3%E4%BB%98.png "在线代付")


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://gitee.com/Joker_zero/recharge/)
3.  你可以 [https://gitee.com/explore](https://gitee.com/Joker_zero/recharge/) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/Joker_zero/recharge/) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/Joker_zero/recharge/)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/Joker_zero/recharge/)
