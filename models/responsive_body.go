/***************************************************
 ** @Desc : This file for 查询响应结果
 ** @Time : 2019.04.08 9:34 
 ** @Author : Joker
 ** @File : responsive_body
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.08 9:34
 ** @Software: GoLand
****************************************************/
package models

// 先锋代付余额查询返回参数
type XFQueryResponseBody struct {
	ResCode         string `json:"resCode"`
	ResMessage      string `json:"resMessage"`
	Balance         string `json:"balance"`         //账户总余额单位（分）
	FreezeAmount    string `json:"freezeAmount"`    //出款在途金额（不可用）单位（分）
	Available       string `json:"available"`       //可用金额（总金额-冻结金额）单位（分）
	NonAdvance      string `json:"nonAdvance"`      //代发不会产生垫资的金额，单位（分）
	PreviousBalance string `json:"previousBalance"` //期初余额，单位（分）
}

// 先锋充值响应返回参数
type XFRechargeResponseBody struct {
	MerchantId string `json:"merchantId"` //商户号
	MerchantNo string `json:"merchantNo"` //商户订单号
	TradeNo    string `json:"tradeNo"`    //先锋 交易订单号
	Status     string `json:"status"`     //订单状态
	TradeTime  string `json:"tradeTime"`  //交易完成时间
	Memo       string `json:"memo"`       //保留域
	ResCode    string `json:"resCode"`    //应答码
	ResMessage string `json:"resMessage"` //应答信息
	Sign       string `json:"sign"`       //订单签名数据
}

// 先锋代付响应返回参数
type XFPayResponseBody struct {
	MerchantId string `json:"merchantId"` //商户号
	MerchantNo string `json:"merchantNo"` //商户订单号
	Amount     string `json:"amount"`     //金额
	TransCur   string `json:"transCur"`   //币种
	TradeNo    string `json:"tradeNo"`    //交易订单号
	TradeTime  string `json:"tradeTime"`  //交易完成时间
	Status     string `json:"status"`     //订单状态
	Memo       string `json:"memo"`       //保留域
	ResCode    string `json:"resCode"`    //应答码
	ResMessage string `json:"resMessage"` //应答信息
	TradeType  string `json:"tradeType"`  //交易类型
	Sign       string `json:"sign"`       //订单签名数据
	ErrorCode  string `json:"errorCode"`  //应答码
	ErrorMsg   string `json:"errorMsg"`   //应答信息
}
