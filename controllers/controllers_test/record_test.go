/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019.05.23 15:12 
 ** @Author : Joker
 ** @File : record_test
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.05.23 15:12
 ** @Software: GoLand
****************************************************/
package controllers_test

import (
	"fmt"
	"github.com/tealeg/xlsx"
	"os"
	"testing"
	"time"
)

func TestCreateXLXS(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("并发竞争，访问了不存在的目录！")
			time.Sleep(1 * time.Second)
		}
	}()

	path := "static/excel/record/"
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			err := os.RemoveAll(path)
			fmt.Println("111:", err)
			os.Mkdir(path, os.ModePerm)
		} else {
			err := os.Mkdir(path, os.ModePerm)
			fmt.Println("2222:", err)
		}
	} else {
		err1 := os.RemoveAll(path)
		fmt.Println("3333335:", err1)
		os.Mkdir(path, os.ModePerm)
	}
	fmt.Println("3333:", err)
}

func TestMakeFile(t *testing.T) {
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row, row1, row2 *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file = xlsx.NewFile()
	sheet, err = file.AddSheet("这是测试")
	if err != nil {
		fmt.Printf(err.Error())
	}
	row = sheet.AddRow()
	row.SetHeightCM(1)
	cell = row.AddCell()
	cell.Value = "姓名"
	cell = row.AddCell()
	cell.Value = "年龄"

	row1 = sheet.AddRow()
	row1.SetHeightCM(1)
	cell = row1.AddCell()
	cell.Value = "狗子"
	cell = row1.AddCell()
	cell.Value = "18"

	row2 = sheet.AddRow()
	row2.SetHeightCM(1)
	cell = row2.AddCell()
	cell.Value = "蛋子"
	cell = row2.AddCell()
	cell.Value = "28"

	err = file.Save("static/excel/record/test_write.xlsx")
	if err != nil {
		fmt.Printf(err.Error())
	}
}

func TestTimer(t *testing.T) {
	tk1 := time.NewTicker(10 * time.Second)
	tk2 := time.NewTicker(1 * time.Second)
	b:=false
	for  {
		select {
		case <-tk1.C:
			fmt.Println("crazy test!")
			tk1.Stop()
			b=true
		case <-tk2.C:
			fmt.Println("are you crazy!")
			tk2.Stop()
		}
		if b {
			break
		}
	}
}
